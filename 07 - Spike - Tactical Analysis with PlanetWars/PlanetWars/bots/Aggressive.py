class Aggressive(object):
    known_enemy_planet_ids = []
    my_planet_ids_cache = []
    planet_ids_to_attack = []
    alternate_attacked_planets = []
    opening_move = True
    wait = 0
    TURNS_TO_WAIT = 10
    def update(self, gameinfo):
        # unique opening move that doesn't use later logic
        if self.opening_move:
            # for first frame where update is called, the gameinfo memebrs aren't yet populated, so we wait
            if not gameinfo.my_planets:
                return
            
            dest = next((x for x in gameinfo.not_my_planets.values() if x.num_ships == 100), None)
            if dest == None:
                self.opening_move = True
                return
            src = next(iter(gameinfo.my_planets.values()))
            gameinfo.planet_order(src, dest, src.num_ships - 1)
            self.known_enemy_planet_ids.append(dest.id)
            # already attacked, therefore we don't need to add it to the planet ids to attack list
            self.my_planet_ids_cache.append(src.id)
            self.opening_move = False
            return

        #current collection of my planets
        my_planets = gameinfo.my_planets.values()

        # remove any planets we've acquired from the known enemy planets list
        for x in my_planets:
            if x.id not in self.my_planet_ids_cache:
                if x.id in self.known_enemy_planet_ids:
                    self.known_enemy_planet_ids.remove(x.id)
        # determine if we've lost any planets since the previous update and add them to the known enemy list
        for x in self.my_planet_ids_cache:
            if gameinfo.planets[x] not in my_planets:
                self.known_enemy_planet_ids.append(x)
                self.planet_ids_to_attack.append(x)
        # update the my planet ids cache
        self.my_planet_ids_cache.clear()
        for x in my_planets:
            self.my_planet_ids_cache.append(x.id)
        
        # add any new enemy planets to our list based on the source planet for any enemy fleets we can see
        for x in gameinfo.enemy_fleets.values():
            src_planet = x.src
            dest_planet = x.dest
            # Check if the destination "planet" is, in fact, a planet
            # Then, if it isn't a planet we knew already belonged to the enemy or ourselves, flag it as a new enemy planet
            if dest_planet in gameinfo.planets and dest_planet.id not in self.known_enemy_planet_ids and dest_planet not in my_planets:
                self.known_enemy_planet_ids.append(dest_planet.id)
                self.planet_ids_to_attack.append(dest_planet.id)
            # Check if the source "planet" is, in fact, a planet
            # Then, if it isn't a planet we knew already belonged to the enemy or ourselves, flag it as a new enemy planet
            if src_planet in gameinfo.planets and src_planet.id not in self.known_enemy_planet_ids and src_planet not in my_planets:
                self.known_enemy_planet_ids.append(src_planet.id)
                self.planet_ids_to_attack.append(src_planet.id)
            
        # and any new planets we can see
        for x in gameinfo.enemy_planets.values():
            if x.id not in self.known_enemy_planet_ids:
                self.known_enemy_planet_ids.append(x.id)
                self.planet_ids_to_attack.append(x.id)
        
        # decrement wait timer
        if self.wait > 0:
            self.wait -= 1

        # launch an offensive, if appropriate
        if self.wait <= 0:
            dest_id = next(iter(self.planet_ids_to_attack), None)
            if dest_id == None:
                self.alternative_action(gameinfo)
                return
            while gameinfo.planets[dest_id] in my_planets:
                self.planet_ids_to_attack.remove(dest_id)
                dest_id = next(iter(self.planet_ids_to_attack), None)
                if dest_id == None:
                    self.alternative_action(gameinfo)
                    return
            self.full_attack(dest_id, gameinfo)
            self.planet_ids_to_attack.remove(dest_id)


    def full_attack(self, dest_id, gameinfo):
        dest = gameinfo.planets[dest_id]
        for src in gameinfo.my_planets.values():
                gameinfo.planet_order(src, dest, src.num_ships - 1)
        self.wait = self.TURNS_TO_WAIT

    def alternative_action(self, gameinfo):
        # approximate factor to turn distance into steps needed
        APPROX_STEP_FACTOR = 5
        # ships needed on top of current num_ships per step needed (to account for growth)
        SHIPS_PER_STEP = 8
        # already targeted planets
        #alternate_attacked_planets = []

        my_planets = gameinfo.my_planets.values()
        for planet in my_planets:
            # how many ships the planet has left after sending out fleets
            running_ship_total = planet.num_ships
            for other_planet in gameinfo.not_my_planets.values():
                if other_planet in self.alternate_attacked_planets:
                    # if we haven't exhausted all of the planets yet, continue the loop to target not yet targeted planets
                    if len(self.alternate_attacked_planets) < len(gameinfo.not_my_planets.values()):
                        continue
                    # if we have exhausted all posible targets, reset the attacked_planets list and start again
                    self.alternate_attacked_planets.clear()
                dist_to_other_planet = planet.distance_to(other_planet)
                approx_steps_needed = int(dist_to_other_planet / APPROX_STEP_FACTOR)
                ships_needed = int(other_planet.num_ships + approx_steps_needed * SHIPS_PER_STEP)
                if running_ship_total - ships_needed > 1:
                    gameinfo.planet_order(planet, other_planet, ships_needed)
                    running_ship_total -= ships_needed
                    self.alternate_attacked_planets.append(other_planet)


