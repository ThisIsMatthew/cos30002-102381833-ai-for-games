from random import choice

class Infection(object):

    def update(self, gameinfo):
        # check if we should attack
        if gameinfo.my_planets and gameinfo.not_my_planets:
            # select random target and destination
            target_weak_count = 5
            src = max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)
            dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(src))
            self.RedirectFleets(gameinfo)
            # launch new fleet if there's enough ships
            if src.num_ships > 1 and target_weak_count > 0:
                gameinfo.planet_order(src, dest, int(src.num_ships * 0.80))
                target_weak_count -= 1
            else:
                self.TargetWeakPoint()
                target_weak_count +=5


    def TargetWeakPoint(self, src, gameinfo):

        dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.num_ships)
        gameinfo.planet_order(src, dest, int(src.num_ships * 0.50))\


    def RedirectFleets(self, gameinfo):
        for fleets in list(gameinfo.my_fleets.values()):
            if (min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(fleets.src))).num_ships <50:
                print("redirecting")
                dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(fleets.src))
                gameinfo.fleet_order(fleets, dest, fleets.num_ships)






