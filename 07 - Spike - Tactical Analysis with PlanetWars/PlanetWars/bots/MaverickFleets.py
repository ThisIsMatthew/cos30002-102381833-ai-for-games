from random import choice

class MaverickFleets(object):

    opening_move = True

    def update(self, gameinfo):
        # check if we should attack
        if gameinfo.my_planets and gameinfo.not_my_planets:
            # select random target and destination
            dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.num_ships)
            src = max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)
            # launch new fleet if there's enough ships
            if src.num_ships > 10:
                gameinfo.planet_order(src, dest, int(src.num_ships * 0.75))
            for fleets in list(gameinfo.my_fleets.values()):
                if (min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(fleets.src))).num_ships < 50:
                    print("redirecting")
                    dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.distance_to(fleets.src))
                    gameinfo.fleet_order(fleets, dest, fleets.num_ships)












