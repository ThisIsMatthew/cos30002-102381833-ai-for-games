class Spreader(object):
    OpeningMove = True
    wait = 0
    SPREAD_WAIT = 6
    attacked_planets = []

    def update(self, gameinfo):
        if self.OpeningMove:
            if not gameinfo.my_planets:
                return
            # for the first move, use spread only with neutral planets
            self.spread(gameinfo, False)
            self.OpeningMove = False
            return
        
        if self.wait <= 0:
            # If there are still a good amount of neutral planets avaliable, shoot for the neutral planets
            if len(gameinfo.neutral_planets) > 0.15 * len(gameinfo.planets):
                self.spread(gameinfo, False)
            # Otherwise, include enemy planets in calculations as well
            self.spread(gameinfo)
        
        self.wait -= 1


    def spread(self, gameinfo, inc_enemy = True):
        # approximate factor to turn distance into steps needed
        APPROX_STEP_FACTOR = 5
        # ships needed on top of current num_ships per step needed (to account for growth)
        SHIPS_PER_STEP = 8
        # Percentage of ships to keep on source planets
        SAFETY_THRESHOLD = 0.15
        # Number of ships to leave on captured planets
        CAPTURED_STRENGTH = 10
        # Percentage of ships to send from planets that don't make an attack to our planet with the most
        BOLSTER_PERCENTAGE = 0.10

        my_planets = gameinfo.my_planets.values()
        # go through my planets to establish targets (starting with plantes with lowest ships so the planets with more ships can target the bigger planets)
        for planet in sorted(my_planets, key=lambda x: -x.num_ships):
            # establish a flag for whether or not this planet makes an attack
            has_attacked = False
            # how many ships the planet has left after sending out fleets
            running_ship_total = planet.num_ships
            if inc_enemy:
                eligible_targets = sorted(gameinfo.not_my_planets.values(), key=lambda x: x.num_ships)
            else:
                eligible_targets = sorted(gameinfo.neutral_planets.values(), key=lambda x: x.num_ships)
            for other_planet in eligible_targets:
                # Check if we've target this planet yet
                if other_planet in self.attacked_planets:
                    # if we haven't yet exhausted the list of eligible targets, move on to one that has yet to be attacked
                    if len(self.attacked_planets) < len(eligible_targets):
                        continue
                    # if we've attacked them all, start over again
                    self.attacked_planets.clear()
                if inc_enemy:
                    dist_to_other_planet = planet.distance_to(other_planet)
                    approx_steps_needed = int(dist_to_other_planet / APPROX_STEP_FACTOR)
                    ships_needed = int(other_planet.num_ships + CAPTURED_STRENGTH + approx_steps_needed * SHIPS_PER_STEP)
                else:
                    ships_needed = other_planet.num_ships + CAPTURED_STRENGTH
                if running_ship_total - ships_needed >= planet.num_ships * SAFETY_THRESHOLD:
                    gameinfo.planet_order(planet, other_planet, ships_needed)
                    running_ship_total -= ships_needed
                    self.attacked_planets.append(other_planet)
                    has_attacked = True
            if not has_attacked:
                # if the planet hasn't made any attacks, bolster whichever of our planets are the strongest with some of our ships
                dest = max(my_planets, key=lambda x: x.num_ships)
                if dest is not planet:
                    gameinfo.planet_order(planet, dest, int(planet.num_ships * BOLSTER_PERCENTAGE))
        self.wait = self.SPREAD_WAIT