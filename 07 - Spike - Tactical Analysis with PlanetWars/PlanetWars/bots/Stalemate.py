class Stalemate(object):
    wait = 2
    opening_move = True
    send_to = None
    other_dest = None
    def update(self, gameinfo):
        if self.opening_move:
            if not gameinfo.my_planets:
                return
            src = next(iter(gameinfo.my_planets.values()))
            self.other_dest = max(gameinfo.planets.values(), key = lambda x : x.distance_to(src))
            self.send_to = max(gameinfo.planets.values(), key = lambda x : x.distance_to(self.other_dest))
            gameinfo.planet_order(src, self.other_dest, 99)
            self.opening_move = False
            return
        self.wait -= 1
        if self.wait <= 0:
            src_fleet = max(iter(gameinfo.my_fleets.values()))
            gameinfo.fleet_order(src_fleet, self.send_to, 99)
            temp = self.other_dest
            self.other_dest = self.send_to
            self.send_to = temp