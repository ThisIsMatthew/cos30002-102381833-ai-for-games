'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform

AGENT_MODES = {
    KEY._1: 'seek',
    KEY._8: 'wander',
    KEY._9: 'flock',
    KEY._0: 'hunt',
}



class Agent(object):

    NEIGHBOUR_R = 150
    SEPARATION_R = 50
    SEPARATION_SCALE = 150
    COHESION_SCALE = 0.5
    ALIGNMENT_SCALE = 50

    def __init__(self, world=None, scale=10.0, mass=1.0, mode='seek'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass
        # Flocking Constants make static Variable agent.
        self.tagged = False;
        # data for drawing this agent
        self.color = 'GREEN'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]
        # Wander Initilisations
        self.wander_target = Vector2D(1,0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale
        # limits?
        self.max_speed = 20 * scale
        self.max_force = 500.0
        # debug draw info?
        self.show_info = False

    def calculate(self, delta):
        # calculate the current steering force
        mode = self.mode
        if mode == 'seek':
            force = self.seek(self.world.target)
        elif mode == 'flock':
            force = self.flock(delta)
        elif mode == 'wander':
            force = self.wander(delta)
        else:
            force = Vector2D()
        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied

        force = self.calculate(delta)  # <-- delta needed for wander
        force.truncate(self.max_force)
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''

        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

        # Flocking RENDERING INFO
        # if self.tagged:
        #     egi.blue_pen()
        #     egi.circle(self.pos, Agent.NEIGHBOUR_R)
        #     egi.set_pen_color(name="RED")
        #     egi.circle(self.pos, self.SEPARATION_R)


        # add some handy debug drawing info lines - force and velocity
        if self.show_info:

            if self.mode == 'wander':
                # Calculates the center of the wander circle in front of the Agent
                wnd_pos = Vector2D(self.wander_dist, 0)
                wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)

                # Draw the wander circle
                egi.green_pen()
                egi.circle(wld_pos, self.wander_radius)

                # Draw the Wander Target (the little circle that moves along the big wander circle)
                egi.red_pen()
                wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
                wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
                egi.circle(wld_pos, 3)

            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def tag_neighbours(self):
        '''tagging other agents in a radius around an agent,'''
        group = [self]
        self.tagged = False
        for agent in self.world.agents:
            if (agent != self) and (self.pos.distance(agent.pos) <= Agent.NEIGHBOUR_R):
                self.tagged = True
                agent.tagged = True
                group.append(agent)
        return group

    def alignment(self, group):
        AvgHeading = Vector2D()
        AvgCount = 0
        for agent in group:
            if agent != self:
                AvgHeading += agent.heading
                AvgCount += 1
        if AvgCount > 0:
            AvgHeading /= float(AvgCount)
        return AvgHeading

    def separation(self, group):
        steering_force = Vector2D()
        for agent in group:
            if (agent != self) and (self.pos.distance(agent.pos) <= Agent.SEPARATION_R):
                dist = self.pos - agent.pos
                steering_force += dist.normalise() / dist.length()
        return steering_force

    def cohesion(self, group):
        CentreMass = Vector2D()
        SteeringForce = Vector2D()
        AvgCount = 0
        for agent in group:
            if agent != self:
                CentreMass += agent.pos
                AvgCount += 1
        if AvgCount > 0:
            CentreMass /= float(AvgCount)
            SteeringForce = self.seek(CentreMass)
        return SteeringForce

    def flock(self, delta):
        group = self.tag_neighbours()
        alignment = self.alignment(group) * Agent.ALIGNMENT_SCALE
        separation = self.separation(group) * Agent.SEPARATION_SCALE
        cohesion = self.cohesion(group) * Agent.COHESION_SCALE
        # egi.green_pen()
        # egi.line_with_arrow(self.pos, self.pos + alignment, 5)
        # egi.red_pen()
        # egi.line_with_arrow(self.pos, self.pos + separation, 5)
        # egi.white_pen()
        # egi.line_with_arrow(self.pos, self.pos + cohesion, 5)

        if group != [self]:
            return alignment + separation + cohesion
        else:
            return self.wander(delta)

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target
        # this behaviour is dependant on the update rate, so this line must
        # be included when using time independant framerate.
        jitter_tts = self.wander_jitter * delta #this time slice
        # first, add a small random vector to the target's position
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius
        target = wt + Vector2D(self.wander_radius, 0)
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)

        return self.seek(wld_target)





