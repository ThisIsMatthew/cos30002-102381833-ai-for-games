'''Heres my class for implimenting objects for the agents to hide behind, and potentially in the future maybe there could be other objects they could interact with, like food for the little guys, or hazzards...'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


class Terrain(object):

    def __init__(self, world=None, scale=50.0):
       self.world = world
       self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
       self.scale = Vector2D(scale, scale)  # easy scaling of agent size
       self.radius = scale




    def render(self, color = None):
        egi.white_pen()    
        egi.circle(self.pos, self.radius)



