from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform



class Projectile(object):

    def __init__(self, target, starting_pos, world=None, scale=2.0, mass=1.0, type='rifle', ):
        self.world = world
        self.pos = Vector2D(starting_pos.x, starting_pos.y)
        self.scale = Vector2D(scale, scale)  # easy scaling of projectile size
        self.radius = scale
        self.type = type
        self.force = Vector2D()
        dir = radians(random() * 360)
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D()  # current acceleration due to force
        self.mass = mass
        self.max_speed = 20.0 * scale
        self.max_force = 500.0
        self.target = Vector2D(target.x, target.y)

    def render(self, color=None):
        egi.white_pen()
        egi.circle(self.pos, self.radius)

    def calculate(self, delta):
        # calculate the current steering force
        mode = self.type
        if mode == 'rifle':
            force = self.rifle_fire()
        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied

        force = self.calculate(delta)  # <-- delta needed for wander
        force.truncate(self.max_force)
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def rifle_fire(self):
        direction_to_target = (self.pos - self.target).normalise()
        return direction_to_target * 10


