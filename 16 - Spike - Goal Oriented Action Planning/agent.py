'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path
from projectile import Projectile
import copy

AGENT_MODES = {
    KEY._1: 'seek',
    KEY._2: 'arrive_slow',
    KEY._3: 'arrive_normal',
    KEY._4: 'arrive_fast',
    KEY._5: 'flee',
    KEY._6: 'pursuit',
    KEY._7: 'follow_path',
    KEY._8: 'wander',
    KEY._9: 'evade',
    KEY._0: 'execute_plan',
}

class Agent(object):

    DEEPNESS = 3

    # NOTE: Class Object (not *instance*) variables!
    DECELERATION_SPEEDS = {
        'slow': 0.9,
        'normal': 0.5,
        'fast': 0.1,
    }

    def __init__(self, world=None, scale=10.0, mass=1.0, mode='seek'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(100, world.cx-100), randrange(100, world.cy-100))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass
        self.target = None
        self.firing_flag = False
        self.moves = []
        self.plan_step = Agent.DEEPNESS
        self.best_plan_key = None

        # Path Initilisations
        self.path = None
        self.waypoint_threshold = 0.0

        # data for drawing this agent
        self.color = 'GREEN'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

        # Wander Initilisations
        self.wander_target = Vector2D(1,0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale
        # limits?
        self.max_speed = 20.0 * scale
        self.max_force = 500.0

        # debug draw info?
        self.show_info = False



    def calculate(self, delta):
        # calculate the current steering force
        mode = self.mode
        if mode == 'seek':
            force = self.seek(self.world.target)
        elif mode == 'arrive_slow':
            self.color = 'DARK_GREEN'
            force = self.arrive(self.world.target, 'slow')
        elif mode == 'arrive_normal':
            self.color = 'GREEN'
            force = self.arrive(self.world.target, 'normal')
        elif mode == 'arrive_fast':
            self.color = 'AQUA'
            force = self.arrive(self.world.target, 'fast')
        elif mode == 'flee':
            force = self.flee(self.world.target)
        elif mode == 'pursuit':
            force = self.pursuit(self.world.hunter)
        elif mode == 'evade':
            force = self.evade(self.world.hunter)
        elif mode == 'wander':
            force = self.wander(delta)
        elif mode == 'follow_path':
            force = self.follow_path()
        elif mode == 'execute_plan':
            if self is self.world.hunter:
                force = self.execute_plan()
        else:
            force = Vector2D()
        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        # just a reminder of the what the dictionary looks like.
        # self.moves[tuple(previous_moves)] = (distance, tuple(agents_to_add))

        if self is self.world.hunter:
            force = self.calculate(delta)  # <-- execute some plans
            force.truncate(self.max_force)
            # determine the new accelteration
            self.accel = force / self.mass  # not needed if mass = 1.0
            # new velocity
            self.vel += self.accel * delta
            # check for limits of new velocity
            self.vel.truncate(self.max_speed)
            # update position
            self.pos += self.vel * delta
            # update heading is non-zero velocity (moving)
            if self.vel.length_sq() > 0.00000001:
                self.heading = self.vel.get_normalised()
                self.side = self.heading.perp()
            # treat world as continuous space - wrap new position if needed
            self.world.wrap_around(self.pos)
        return

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        # draw the path if it exists and the mode is follow
        if self.path:
           self.path.render()

        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)


        # add some handy debug drawing info lines - force and velocity
        if self.show_info:

            if self.mode == 'wander':
                # Calculates the center of the wander circle in front of the Agent
                wnd_pos = Vector2D(self.wander_dist, 0)
                wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)

                # Draw the wander circle
                egi.green_pen()
                egi.circle(wld_pos, self.wander_radius)

                # Draw the Wander Target (the little circle that moves along the big wander circle)
                egi.red_pen()
                wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
                wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
                egi.circle(wld_pos, 3)

            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)
            # Grid Overlay
            egi.blue_pen()

            x = 0
            y = 0
            fill = False
            size = self.world.CELL_SIZE
            for columns in self.world.grid:
                for cell in columns:
                    if self.pos.is_within_cell(cell, size):
                        fill = True
                        egi.red_pen()
                    else:
                        fill = False
                        egi.blue_pen()
                    # A single quad - TL to TR to BR to BL (to TL...)
                    egi.rect(cell.x, cell.y + size, cell.y + size, cell.y, fill)

            # for columns in self.world.grid:
            #     for cells in columns:
            #         egi.rect()


    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def randomise_path(self):
        cx = self.world.cx  # width
        cy = self.world.cy  # height
        NUMBER_OF_POINTS = 6
        height_margin = cy * (1 / 6)
        width_margin = cx * (1 / 6)
        self.path.create_random_path(NUMBER_OF_POINTS, 0+width_margin, 0+height_margin, cx-width_margin, cy-height_margin)

    def follow_path(self):
        threshold_dist = 100
        pt = self.path.current_pt()

        if self.path.is_finished():
            return self.arrive(pt, 'normal')
        elif self.pos.distance(pt) < threshold_dist:
            self.path.inc_current_pt()
            return self.seek(self.path.current_pt())

        return self.seek(pt)

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def flee(self, hunter_pos):
        ''' move away from hunter position '''
        if self.pos.distance(hunter_pos) < 200:
            desired_vel = -self.seek(hunter_pos)
            return (desired_vel - self.vel)

        return Vector2D()

    def arrive(self, target_pos, speed):
        ''' this behaviour is similar to seek() but it attempts to arrive at
            the target position with a zero velocity'''
        decel_rate = self.DECELERATION_SPEEDS[speed]
        to_target = target_pos - self.pos
        dist = to_target.length()
        if dist > 0:
            # calculate the speed required to reach the target given the
            # desired deceleration rate
            speed = dist / decel_rate
            # make sure the velocity does not exceed the max
            speed = min(speed, self.max_speed)
            # from here proceed just like Seek except we don't need to
            # normalize the to_target vector because we have already gone to the
            # trouble of calculating its length for dist.
            desired_vel = to_target * (speed / dist)
            return (desired_vel - self.vel)
        return Vector2D(0, 0)

    def pursuit(self, pursuit):
        ''' this behaviour predicts where an agent will be in time T and seeks
            towards that point to intercept it. '''
        if self == self.world.hunter:
            return Vector2D()
        distance_ahead = 0.1
        predicted_pos = pursuit.pos + pursuit.vel * distance_ahead
        return self.seek(predicted_pos)


    def evade(self, evader):
        ''' this behaviour predicts where an agent will be in time T and flees
                    away that point to intercept it. '''
        if self == self.world.hunter:
            return Vector2D()
        distance_ahead = 0.1
        predicted_pos = evader.pos + evader.vel * distance_ahead
        return self.flee(predicted_pos)

    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target
        # this behaviour is dependant on the update rate, so this line must
        # be included when using time independant framerate.
        jitter_tts = self.wander_jitter * delta #this time slice
        # first, add a small random vector to the target's position
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius
        target = wt + Vector2D(self.wander_radius, 0)
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)

        return self.seek(wld_target)

    def get_closest_target(self):
        """any will do, its just got to be the closest"""
        return min((agents for agents in self.world.agents if agents is not self), key=lambda x: x.pos.distance(self.pos), default=None)

    def fire(self, shooter, target):

        if shooter is self:
            self.color = "ORANGE"
            self.heading = (target.pos - self.pos).normalise()
            self.side = self.heading.perp()
            self.world.projectiles.append(Projectile("rocket", target, shooter.pos, self.world))

        self.firing_flag = False

    def get_trick_shot(self, target):
        """returns a trick shot, also known as the vector between two agents... oh dear shoudn't happen, but it will """
        oh_dear = None
        for agent in self.world.agents:
            oh_dear = agent
            if agent is not self or target:
                # checks next to see if the marksman is not in between the targets which would be too much movement for a trick shot.
                if (target.pos.x > self.pos.x and agent.pos.x > self.pos.x) or (target.pos.x < self.pos.x and agent.pos.x < self.pos.x):
                    if (target.pos.y > self.pos.y and agent.pos.y > self.pos.y) or (target.pos.y < self.pos.y and agent.pos.y < self.pos.x):
                        print("sick trickshot bro")
                        return agent, True
        print("oh dear no trickshots")
        return oh_dear, False

    def pos_for_trick_shot(self, target):
        other_target = self.get_trick_shot(target)
        if other_target[1]:
            shot_line = (target.pos - other_target[0].pos).normalise()
            if shot_line is not None:
                rot_angle = shot_line.angle_to_pos_x()
                vect_from_target = (self.pos - target.pos)
                vect_from_target.rotate(-rot_angle)
                closest_point = target.pos + shot_line * vect_from_target.x
                return closest_point, other_target[0]
        elif other_target[1] == False:
            shot_line = (target.pos - other_target[0].pos).normalise()
            return target.pos + shot_line * 20, other_target[0]
        return None

    def execute_plan(self):
        if not self.best_plan_key or self.plan_step >= len(self.best_plan_key[0]):
            # lets generate some plans for the next 3 moves
            self.plan_step = 0
            self.moves = []
            self.plan_shots()
            # get the key for the plan with the shortest distance
            self.best_plan_key = min(self.moves, key=lambda x: x[1], default=None)
            print(self.best_plan_key[1])
            self.path = Path()
            self.path.set_pts(self.best_plan_key[0])
        elif self.best_plan_key:
            if self.pos.distance(self.best_plan_key[0][self.plan_step]) > 5:
                return self.seek(self.best_plan_key[0][self.plan_step])
            else:
                self.fire(self, self.best_plan_key[2][self.plan_step])
                # Paths generated just to see plan pathways
                self.plan_step += 1
                if not self.path.is_finished():
                    self.path.inc_current_pt()
        return Vector2D()

    def plan_shots(self, missing_agents=[], previous_moves=[], depth=3, distance=0):
        # first thing is to sink to new depths of a confusing recursive function
        new_depths = depth - 1
        current_agents = [agents for agents in self.world.agents if agents not in missing_agents]
        # next we check if we've hit rock bottom, or if there are no more pairs to trickshot
        # this may destroy everything by removing missing agents from the world list
        if new_depths == 0 or len(current_agents) <= 2:
            # we get every other agent in missing_agents as thats the one we actually shoot at when we execute the plan
            agents_to_add = []
            for i in range(len(missing_agents)):
                if (i + 1) % 2 == 0:
                    agents_to_add.append(missing_agents[i])
            # We add to a dictionary with the key as a tuple of move positions,
            # first value is cumulative distance traveled,
            # and second value is a tuple of the agents to shoot at.
            # we do this very every chained set of shots around an initial target 3 deep!
            self.moves.append((tuple(previous_moves), distance, tuple(agents_to_add)))
            return
        # get some deep copies to reference in this hypothetical timeline
        current_absent_agents = copy.deepcopy(missing_agents)
        current_moves = copy.deepcopy(previous_moves)
        # get a hypothetical position based on where we have been in this timeline
        current_pos = self.pos
        if previous_moves:
            current_pos = previous_moves[-1]
        # look for some nearby targets in this hypothetical radius.
        nearby_targets = []
        # this next line is as upsetting to read as it is succinct
        # loop through every agent that's in the world that isn't in the current absent moves list
        for agent in current_agents:
            if current_pos.distance(agent.pos) < 100:
                nearby_targets.append(agent)
        # now have a look at the hypothetical trickshots around our hypothetical
        for target in nearby_targets:
            # this tuple contains both the closest position we need to get to for a trick shot
            # and the agent we need to shoot
            targets_tuple = self.pos_for_trick_shot(target)
            # add the hypothetical distance we've traveled.
            distance_travelled = distance + current_pos.distance(targets_tuple[0])
            # add the hypothetical positions we've traveled to.
            current_moves.append(targets_tuple[0])
            # store the two targets that have been hypothetically shot so that they don't get shot again
            current_absent_agents.extend([target, targets_tuple[1]])
            # lets go deeper...
            self.plan_shots(current_absent_agents, current_moves, new_depths, distance_travelled)
















