from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path

class Projectile(object):

    def __init__(self,type, target, starting_pos, world=None, mass=1.0,):
        self.world = world
        self.pos = starting_pos.copy()
        self.target = target
        self.type = type

        if type == "rifle":
            self.rifle()
        elif type == "rocket":
            self.rocket()
        elif type == "handgun":
            self.handgun()
        elif type == "grenade":
            self.grenade()


    def render(self, color=None):
        egi.white_pen()
        egi.circle(self.pos, self.radius)

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        self.pos += self.vel * delta
        # self.world.wrap_around(self.pos)

        for agent in self.world.agents:
            if (self.pos.distance(agent.pos) < 20) and (agent != self.world.hunter):
                self.world.agents.remove(agent)

    def rifle(self):
        self.radius = 2
        self.speed = 1000
        target_pos = self.tracking()
        self.vel = (target_pos - self.pos).normalise() * self.speed

    def rocket(self):
        self.radius = 4
        self.speed = 600
        target_pos = self.tracking()
        # predicted position ends up being off screen more often since the rocket travels so slowly...
        self.vel = (target_pos - self.pos).normalise() * self.speed

    def handgun(self):
        self.radius = 2
        self.speed = 1000
        target_pos = self.tracking()
        target_pos.x += randrange(-100,100)
        target_pos.y += randrange(-100,100)
        self.vel = (target_pos - self.pos).normalise() * self.speed

    def grenade(self):
        self.radius = 6
        self.speed = 500
        target_pos = self.tracking()
        target_pos.x += randrange(-100,100)
        target_pos.y += randrange(-100,100)
        self.vel = (target_pos - self.pos).normalise() * self.speed

    def tracking(self):
        """ So lets try and look at the distance of the target agent over  a small amount of time.
         Then if the agent is closer, we reduce that time, and if its further increase it.
         Then we use that modifier to make the predicted position more accurate... hopefully """
        distance_to_target = self.pos.distance(self.target.pos)
        base_time_to_target = distance_to_target / self.speed
        prediction_target = self.target.pos.copy()
        predicted_pos = prediction_target + (self.target.vel * base_time_to_target)
        predicted_distance = self.pos.distance(predicted_pos)
        # the larger the sensitivty the more we will account for the agent getting closer or further away
        sensitivity = 1.1
        # gives a a number greater than 1 if moving further away and less than 1 if moving closer.
        time_modifier = predicted_distance/distance_to_target
        time_modifier *= sensitivity
        # time modifier grows if moving further away and shrinks when target gets closer
        predicted_time = base_time_to_target * time_modifier
        # returns a more accurate position based on the time modifier given
        hopefully_more_accurate_pos = prediction_target + (self.target.vel * predicted_time)
        return hopefully_more_accurate_pos


