'''Heres my class for implimenting objects for the agents to hide behind, and potentially in the future maybe there could be other objects they could interact with, like food for the little guys, or hazzards...'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from agent import Agent
import copy


class Hive(object):

    def __init__(self, starting_pos, world=None, scale=50.0):
       self.world = world
       self.pos = starting_pos
       self.scale = Vector2D(scale, scale)  # easy scaling of agent size
       self.radius = scale
       self.pollen = 0

    def update(self):
        # if the bees have delivered enough pollen the hive makes a new bee
        if self.pollen >= 10:
            print ('bee produced')
            new_bee = Agent(self.world,10,1)
            new_bee.hive = self
            new_bee.pos = copy.deepcopy(self.pos)
            self.world.agents.append(new_bee)
            self.pollen = 0


    def render(self, color = None):
        egi.white_pen()  
        egi.circle(self.pos, self.radius)

        



