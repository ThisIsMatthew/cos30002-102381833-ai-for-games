from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path


class Flower(object):

    def __init__(self, world=None, scale=10.0):
       self.world = world
       self.pos = self.world.grid.get_random_cell_pos()
       self.pollen = 10
       self.scale = Vector2D(scale, scale)  # easy scaling of agent size
       self.radius = scale

    def update(self):
        if self.pollen <= 0:
            self.world.flowers.remove(self)
            # if this was the last flower make sure another one pops up
            if self.world.flowers == []:
                self.world.flowers.append(Flower(self.world))
            # reset the bees who were targetting this removed flower
            for agent in self.world.agents:
                agent.target = None
           
    def render(self, color = None):
        if self.pos:
            egi.green_pen()    
            egi.cross(self.pos, self.radius)



