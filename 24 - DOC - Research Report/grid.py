from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path

class Grid(object):

    def __init__(self, cell_size = 20.0, world = None):
        '''divides the world up into a grid of navigation tiles'''
        self.cell_size = cell_size
        self.world = world
        self.offset = cell_size * 0.5
        self.gridArray = self.world_to_grid()
        
    def world_to_grid(self):
        '''turns the world position into a 2D array containing a dictionary of information about that cell
        currently that infor is a vector 2d of its centre position, and a boolean of whether it is passable'''
        CELLS = []
        pos_x = 0
        pos_y = 0
        cell_x = 0
        cell_y = 0
        while pos_x < self.world.cx + self.cell_size:
            new_column = []
            while pos_y < self.world.cy:
                # A dictionary that contains a 2D Vector
                new_column.append(dict(position=Vector2D(pos_x + self.offset, pos_y + self.offset), blocked=False))
                pos_y += self.cell_size
                cell_y += 1
            CELLS.append(new_column)
            pos_x += self.cell_size
            cell_x += 1
            pos_y = 0
        return CELLS

    def is_within_cell(self, pos, cell):
        '''returns true if the supplied Vector2D is inside a square cell.'''
        px, py, cx, cy = pos.x, pos.y, cell['position'].x - self.offset, cell['position'].y - self.offset
        if (px > cx and px < cx + self.cell_size) and (py > cy and py < cy + self.cell_size):
            return True
        else:
            return False
    
    def position_to_cell(self, v2):
        '''takes a vector2D and the grid and returns a tuple of the cell coordinates'''
        x=0
        y=0
        for columns in self.gridArray:
            for cell in columns:
                if self.is_within_cell(v2,cell):
                    return (x,y)
                y+=1
            x+=1
            y=0
        return 0
    
    def get_random_cell_pos(self):
        '''generates a random cell position in the middle of the map, if its blocked it runs it again'''
        grid_len = len(self.gridArray)
        rand_cell = self.gridArray[randrange(5,grid_len-5)][randrange(5,grid_len-5)]
        if rand_cell['blocked']:
            self.get_random_cell_pos
        else:
            return rand_cell['position']
    
    def set_cell_blocked(self, target_cell, blocked=True):
        for columns in self.gridArray:
            for cell in columns:
                if cell is target_cell:
                    cell['blocked'] = blocked
     


        

