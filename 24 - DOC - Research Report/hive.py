'''Heres my class for implimenting objects for the agents to hide behind, and potentially in the future maybe there could be other objects they could interact with, like food for the little guys, or hazzards...'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from agent import Agent
import copy


class Hive(object):

    def __init__(self, starting_pos, world=None, scale=50.0):
       self.world = world
       self.pos = starting_pos
       self.scale = Vector2D(scale, scale)  # easy scaling of agent size
       self.radius = scale
       self.pollen = 0
       self.bees_in_hive = []
       self.hornets_in_hive = 0
       self.total_pollen_gathered = 0
       self.hiveID = randrange(0,100)
       self.previous_pollen = 0 
       self.counter = 100
       self.log_step = 0

    def update(self):
        # if the bees have delivered enough pollen the hive makes a new bee
        if self.counter <= 0:
            self.counter +=100
            self.bees_in_hive = []
            for agent in self.world.agents:
                if agent.hive == self:
                    self.bees_in_hive.append(agent)
            self.log_step += 1
            self.log_beehavior(self.log_step,self.hiveID,len(self.bees_in_hive), self.hornets_in_hive,self.total_pollen_gathered)
        else:
            self.counter -= 0.2

    def recieve_pollen(self, bee):
        self.pollen += bee.pollen
        bee.pollen -= bee.pollen
        if self.pollen >= 4:
            self.total_pollen_gathered += 4
            self.produce_bee()
            self.pollen = 0
        
    def produce_bee(self):
        new_bee = Agent(self.world,10,1)
        new_bee.hive = self
        new_bee.pos = copy.deepcopy(self.pos)
        if len(self.bees_in_hive)> 3 and self.hornets_in_hive < 2:
            self.hornets_in_hive += 1
            new_bee.not_hornet = False
        self.world.agents.append(new_bee)

    def render(self, color = None):
        egi.white_pen()  
        egi.circle(self.pos, self.radius)

    def log_beehavior(self, log_step, hiveID, bees_in_hive, hornets_in_hive, total_pollen_gathered):
        data_file = open("bee_data.csv", "a")
        data_file.write(str(log_step)+','+str(hiveID)+','+str(bees_in_hive)+','+str(hornets_in_hive)+','+str(total_pollen_gathered))
        data_file.write("\n")

        



