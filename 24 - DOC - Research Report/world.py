'''A 2d world that supports agents with steering behaviour

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from matrix33 import Matrix33
from graphics import egi
from grid import Grid
from flower import Flower
from hive import Hive


class World(object):

    CELL_SIZE = 20.0

    def __init__(self, cx, cy):
        self.cx = cx
        self.cy = cy
        self.target = Vector2D(cx / 2, cy / 2)
        self.hunter = None
        self.agents = []
        self.paused = True
        self.show_info = True
        self.hunter_index = 0
        self.flowers = []
        self.show_grid = False
        self.grid = Grid(World.CELL_SIZE, self)
        self.flower_timer = 100
        hive_pos_one = self.grid.gridArray[3][3]['position']
        grid_len = len(self.grid.gridArray)
        hive_pos_two = self.grid.gridArray[grid_len-4][grid_len-4]['position']
    
        self.hives = [Hive(hive_pos_one, self, 20), Hive(hive_pos_two, self, 20)]

    def update(self, delta):
        if not self.paused:

            for flower in self.flowers:
                flower.update()
            if self.flower_timer > 0:
                self.flower_timer -= delta * 5
            else:
                self.flower_timer = 100
                self.flowers.append(Flower(self))

            for agent in self.agents:
                agent.update(delta)
            
            for hive in self.hives:
                hive.update()


    def render(self):
        
        for agent in self.agents:
            agent.render()

        for hive in self.hives:
            hive.render()
        
        for flower in self.flowers:
            flower.render()

        if self.target:
            egi.red_pen()
            egi.cross(self.target, 10)
        
        size = self.CELL_SIZE
        offset = self.CELL_SIZE * 0.5
        for columns in self.grid.gridArray:
            for cell in columns:
                if cell['blocked']:
                    egi.red_pen()
                    egi.circle(cell['position'], 8)

        if self.show_grid:
             # Grid Overlay
            size = self.CELL_SIZE
            offset = self.CELL_SIZE * 0.5
            for columns in self.grid.gridArray:
                for cell in columns:
                    if cell['blocked']:
                        egi.red_pen()
                        egi.cross(cell['position'], 5)
                    else:
                        egi.white_pen()
                        egi.line(cell['position'].x - offset, cell['position'].y - offset, cell['position'].x + size - offset, cell['position'].y - offset)
                        egi.line(cell['position'].x - offset, cell['position'].y - offset, cell['position'].x - offset, cell['position'].y + size - offset)
                        egi.line(cell['position'].x + size - offset, cell['position'].y + size - offset, cell['position'].x + size - offset, cell['position'].y - offset)
                        egi.line(cell['position'].x + size - offset, cell['position'].y + size - offset, cell['position'].x - offset, cell['position'].y + size - offset)
         


        if self.show_info:
            infotext = ', '.join(set(agent.mode for agent in self.agents))
            egi.white_pen()
            egi.text_at_pos(0, 0, infotext)

    def wrap_around(self, pos):
        ''' Treat world as a toroidal space. Updates parameter object pos '''
        max_x, max_y = self.cx, self.cy
        if pos.x > max_x:
            pos.x = pos.x - max_x
        elif pos.x < 0:
            pos.x = max_x - pos.x
        if pos.y > max_y:
            pos.y = pos.y - max_y
        elif pos.y < 0:
            pos.y = max_y - pos.y

    def transform_points(self, points, pos, forward, side, scale):
        ''' Transform the given list of points, using the provided position,
            direction and scale, to object world space. '''
        # make a copy of original points (so we don't trash them)
        wld_pts = [pt.copy() for pt in points]
        # create a transformation matrix to perform the operations
        mat = Matrix33()
        # scale,
        mat.scale_update(scale.x, scale.y)
        # rotate
        mat.rotate_by_vectors_update(forward, side)
        # and translate
        mat.translate_update(pos.x, pos.y)
        # now transform all the points (vertices)
        mat.transform_vector2d_list(wld_pts)
        # done
        return wld_pts

    def transform_point(self, point, pos, forward, side):
        '''transforms a single given point, using provided position,
        and direction (forward and side unit vectors), to object world space.'''
        # make copy of the original point (so we dont trash it)
        wld_pt = point.copy()
        # create a transformation matrix to perform the operations
        mat = Matrix33()
        # rotate
        mat.rotate_by_vectors_update(forward, side)
        # and translate
        mat.translate_update(pos.x, pos.y)
        # now transform the point (in place)
        mat.transform_vector2d(wld_pt)
        # done
        return wld_pt

   

        


